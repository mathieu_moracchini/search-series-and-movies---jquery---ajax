$(document).ready(function () { 
    $('.bar').hide();
    $('.documentation').hide();
    $('.list').hide();
    //Partie 1
    $('#push').click(function(){
        if ($('#search').val() !== '') { //vider les informations si la barre de recherche est pleine
            $('#info').html('');
        } else {
            
        }

        let recherche = $('#search').val(); //recuperer le texte contenu dans le input

        $.ajax({ //appel AJAX
            url : 'http://www.omdbapi.com/?s='+ recherche +'&apikey=83d5c33c', //83d5c33c
            dataType: 'json',
            success : function (response) {
                console.log(response);
                $('#container').html(''); //vider le contenu à chaque nouvelle recherche
                $('.list').removeClass('hidden');
                for (i = 0 ; i < response.Search.length ; i++) {
                    $('#container').append('<div class=\"items\"><img class=\"picture\" data-id=\"'+ response.Search[i].imdbID +'\" class=\"' + response.Search[i].Title + '\" src=\" ' + response.Search[i].Poster + ' \"><h3 class=\"title_movie\">' + response.Search[i].Title + '</h3></div>');
                }
                setTimeout(function () {
                    $('.list').slideDown(); //effet entrant
                });
                //Partie 2
                $('.picture').click(function() {
                    let action = $(event.target).attr('data-id');
                    
                    $.ajax({ //appel AJAX
                        url : 'http://www.omdbapi.com/?i='+ action +'&apikey=83d5c33c', 
                        dataType: 'json',
                        success : function (response) {
                            console.log(response);
                            $('.list').addClass('hidden');
                            $('.items').addClass('hidden'); //tout cacher
                            $('#search').val(response.Title); //placer le titre dans la barre de recherche
                            $('#info').removeClass('hidden'); //supprimer la classe hidden
                            $('#poster').removeClass('hidden'); //supprimer la classe hidden
                            //Informations
                            $('#poster').append('<img class=\"img_info\" src=\" ' + response.Poster + ' \">');
                            $('#info').append('<h2 class=\"space\"><span>Titre : </span>' + response.Title + '</h2>');
                            $('#info').append('<p class=\"space\"><span>Année : </span>' + response.Year + '<p>');
                            $('#info').append('<p class=\"space\"><span>Durée : </span>' + response.Runtime + '<p>');
                            $('#info').append('<p class=\"space\"><span>Genre : </span>' + response.Genre + '<p>');
                            $('#info').append('<p class=\"space\"><span>Réalisateurs : </span>' + response.Writer + '<p>');
                            $('#info').append('<p class=\"space\"><span>Acteurs : </span>' + response.Actors + '<p>');
                            $('#info').append('<p class=\"space\"><span>Résumé : </span>' + response.Plot + '<p>');
                            $('#info').append('<p class=\"space\"><span>Awards : </span>' + response.Awards + '<p>');
                            $('#info').append('<p class=\"space\"><span>Type : </span>' + response.Type + '<p>');
                            if (response.Type === 'series') {
                                $('#info').append('<p class=\"space\"><span>Saisons : </span>' + response.totalSeasons + '<p>');
                            } else {
                                $('#info').append('');
                            }
                            for (e = 0 ; e < response.Ratings.length ; e++) {
                                $('#info').append('<p><span class=\"space\">Critiques : </span><br>' + response.Ratings[e].Source + ' : ' +response.Ratings[e].Value + '<p>');
                            }
                            setTimeout(function () {
                                $('.documentation').slideDown(); //effet entrant
                            });
                        
                        }
                    });
                    
                });
            }
        });
    });

    setTimeout(function () {
        $('.bar').fadeIn(); //effet entrant
    } , 500);

});

